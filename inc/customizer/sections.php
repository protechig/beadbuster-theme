<?php
/**
 * Customizer sections.
 *
 * @package BeadBuster 2018
 */

/**
 * Register the section sections.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function bbs__customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'bbs__additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'beadbuster' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'bbs__social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'beadbuster' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'beadbuster' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'bbs__header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'beadbuster' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'bbs__footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'beadbuster' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'bbs__customize_sections' );
