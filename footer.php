<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BeadBuster 2018
 */

?>
</div>
	</div><!-- #content -->

	<footer class="site-footer">
		<div class="footer-widgets">
	     <?php get_sidebar('footer'); ?>
        </div>

		<div class="site-info">
			<?php bbs__display_copyright_text(); ?>
			<?php bbs__display_social_network_links(); ?>
			<div class="social-icons">
			    <a href="<?php the_field('instagram_url', 'option'); ?>" target=_blank><span class="fa-stack fa-lg">
				<i class="fab fa-instagram" style='color:#a52a90;'></i>
				</span></a>
				<a href="<?php the_field('facebook_url', 'option'); ?>" target=_blank ><span class="fa-stack fa-lg">
				<i class="fab fa-facebook" style='color:#3e5895;'></i>
				</span></a>
				<a href="<?php the_field('twitter_url', 'option'); ?>" target=_blank><span class="fa-stack fa-lg">
				<i class="fab fa-twitter" style='color:#1d9def;'></i>
				</span></a>
				<a href="mailto:<?php the_field('email_url', 'option'); ?>" target=_blank><span class="fa-stack fa-lg">
				<i class="fas fa-at" style='color:#ffff;'></i>
				</span></a>
				<a href="<?php the_field('youtube_url', 'option'); ?>" target=_blank><span class="fa-stack fa-lg">
				<!-- <i class="fab fa-youtube" style='color:#e24a3f;'></i> -->
				<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/YouTube.svg';?>">
				</span></a>

             </div>

		</div><!-- .site-info -->
	</footer><!-- .site-footer container-->
</div><!-- #page -->

<?php wp_footer(); ?>

<nav class="off-canvas-container" aria-hidden="true">
	<button type="button" class="off-canvas-close" aria-label="<?php esc_html_e( 'Close Menu', 'beadbuster' ); ?>">
		<span class="close"></span>
	</button>
	<?php
		// Mobile menu args.
		$mobile_args = array(
			'theme_location'  => 'mobile',
			'container'       => 'div',
			'container_class' => 'off-canvas-content',
			'container_id'    => '',
			'menu_id'         => 'mobile-menu',
			'menu_class'      => 'mobile-menu',
		);

		// Display the mobile menu.
		wp_nav_menu( $mobile_args );
	?>
</nav>
<div class="off-canvas-screen"></div>
</body>
</html>
