<?php
/**
 * BeadBuster 2018 functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package BeadBuster 2018
 */

if (!function_exists('bbs__setup')):
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function bbs__setup()
{
        /**
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on BeadBuster 2018, use a find and replace
         * to change 'beadbuster' to the name of your theme in all the template files.
         * You will also need to update the Gulpfile with the new text domain
         * and matching destination POT file.
         */
        load_theme_textdomain('beadbuster', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /**
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /**
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');
        add_image_size('full-width', 1920, 1080, false);
        add_image_size('medium', 790, 370, false);
        add_image_size('related-post-thumbnail', 300, 200, true);

        // Register navigation menus.
        register_nav_menus(array(
            'primary' => esc_html__('Primary Menu', 'beadbuster'),
            'mobile' => esc_html__('Mobile Menu', 'beadbuster'),
        ));

        /**
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('bbs__custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Custom logo support.
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 500,
            'flex-height' => true,
            'flex-width' => true,
            'header-text' => array('site-title', 'site-description'),
        ));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');
    }
endif; // bbs__setup
add_action('after_setup_theme', 'bbs__setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function bbs__content_width()
{
    $GLOBALS['content_width'] = apply_filters('bbs__content_width', 640);
}
add_action('after_setup_theme', 'bbs__content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function bbs__widgets_init()
{

    // Define sidebars.
    $sidebars = array(
        'sidebar-1' => esc_html__('Sidebar 1', 'beadbuster'),
		 'footer-1'  => esc_html__( 'Footer 1', 'beadbuster' ),
		 'footer-2'  => esc_html__( 'Footer 2', 'beadbuster' ),
		 'footer-3'  => esc_html__( 'Footer 3', 'beadbuster' ),
        // 'sidebar-3'  => esc_html__( 'Sidebar 3', 'beadbuster' ),
    );

    // Loop through each sidebar and register.
    foreach ($sidebars as $sidebar_id => $sidebar_name) {
        register_sidebar(array(
            'name' => $sidebar_name,
            'id' => $sidebar_id,
            'description' => /* translators: the sidebar name */sprintf(esc_html__('Widget area for %s', 'beadbuster'), $sidebar_name),
            'before_widget' => '<aside class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ));
    }

}
add_action('widgets_init', 'bbs__widgets_init');

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load styles and scripts.
 */
require get_template_directory() . '/inc/scripts.php';

/**
 * Load custom ACF features.
 */
require get_template_directory() . '/inc/acf.php';

/**
 * Load custom filters and hooks.
 */
require get_template_directory() . '/inc/hooks.php';

/**
 * Load custom queries.
 */
require get_template_directory() . '/inc/queries.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer/customizer.php';

/**
 * Scaffolding Library.
 */
require get_template_directory() . '/inc/scaffolding.php';

function mytheme_add_woocommerce_support()
{
    add_theme_support('woocommerce');
    add_theme_support('wc-product-gallery-zoom');
    add_theme_support('wc-product-gallery-lightbox');
    add_theme_support('wc-product-gallery-slider');

}
add_action('after_setup_theme', 'mytheme_add_woocommerce_support');

add_action('init', 'bbs_single_product_mods');
function bbs_single_product_mods()
{
    //* Remove Breadcrumbs
    remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
    //* Reposition Product Summary
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
    add_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 5);
    add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
    //* Remove Product Sidebar
    remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
    //* Add Product Bottom Section
    add_action('woocommerce_after_single_product_summary', 'bbs_product_bottom', 10);
    remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

}
add_filter('add_to_cart_text', 'woo_custom_single_add_to_cart_text'); // < 2.1
add_filter('woocommerce_product_single_add_to_cart_text', 'woo_custom_single_add_to_cart_text'); // 2.1 +

function woo_custom_single_add_to_cart_text()
{

    return __('Buy Now', 'woocommerce');

}
add_filter('add_to_cart_text', 'woo_custom_product_add_to_cart_text'); // < 2.1
add_filter('woocommerce_product_add_to_cart_text', 'woo_custom_product_add_to_cart_text'); // 2.1 +

function woo_custom_product_add_to_cart_text()
{

    return __('Buy Now', 'woocommerce');

}

function bbs_product_bottom()
{

// check if the repeater field has rows of data
    if (have_rows('product_expander')):
    ?>
	<div class="expander-container">
	<?php

    // loop through the rows of data
    while (have_rows('product_expander')): the_row();
        ?>
					<div class="expander">
							<div class="section-title closed <?php if (get_row_index() == 1) {echo "open row-1";}?>">
								<h2><?php the_sub_field('description_header');?></h2>
								<div class="plusminus">
				  				</div>
							</div>
							<div class="section-content">
								<?php the_sub_field('summary');?>
							</div>
					</div><!-- .expander -->
						<?php
endwhile;
    ?>
	</div><!-- .expander-container -->
	<?php
endif;

// check if the repeater field has rows of data
    if (have_rows('product_video')):
    ?>
	<div class="video-container">
	<?php

    // loop through the rows of data
    while (have_rows('product_video')): the_row();
        ?>
						<div class="video">
							<?php the_sub_field('video');?>
						</div>
						<?php
endwhile;
    ?>
	</div><!-- .video-container -->
	<?php
endif;
}

//* Remove Product Tabs
add_filter('woocommerce_product_tabs', 'woo_remove_product_tabs', 98);

function woo_remove_product_tabs($tabs)
{

    unset($tabs['description']); // Remove the description tab
    unset($tabs['reviews']); // Remove the reviews tab
    unset($tabs['additional_information']); // Remove the additional information tab

    return $tabs;

}

//* Change woocommerce upsells from 4 to 3
add_filter('woocommerce_upsell_display_args', 'custom_woocommerce_upsell_display_args');
function custom_woocommerce_upsell_display_args($args)
{
    $args['posts_per_page'] = 3; // Change this number
    $args['columns'] = 3; // This is the number shown per row.
    return $args;
}

if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false,
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Theme Shop Settings',
        'menu_title' => 'Shop',
        'parent_slug' => 'theme-general-settings',
    ));

}
// the excerpt shortner
function excerpt($limit)
{
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    $excerpt = preg_replace('`[[^]]*]`', '', $excerpt);
    return $excerpt;
}

/**
 * Ensure cart contents update when products are added to the cart via AJAX
 */
function my_header_add_to_cart_fragment( $fragments ) {
 
    ob_start();
    $count = WC()->cart->cart_contents_count;
    ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php
    if ( $count > 0 ) {
        ?>
        <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
        <?php            
    }
        ?></a><?php
 
    $fragments['a.cart-contents'] = ob_get_clean();
     
    return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'my_header_add_to_cart_fragment' );

//MEGAMENU
function megamenu_add_theme_beadbuster_1547057362($themes) {
    $themes["beadbuster_1547057362"] = array(
        'title' => 'Beadbuster',
        'container_background_from' => 'rgb(255, 255, 255)',
        'container_background_to' => 'rgb(255, 255, 255)',
        'container_padding_top' => '10px',
        'container_padding_bottom' => '10px',
        'arrow_up' => 'dash-f347',
        'menu_item_background_from' => 'rgb(255, 255, 255)',
        'menu_item_background_to' => 'rgb(255, 255, 255)',
        'menu_item_background_hover_from' => 'rgb(208, 2, 27)',
        'menu_item_background_hover_to' => 'rgb(208, 2, 27)',
        'menu_item_link_color' => 'rgb(34, 34, 34)',
        'menu_item_link_padding_top' => '10px',
        'menu_item_link_padding_bottom' => '15px',
        'menu_item_border_color' => 'rgb(255, 255, 255)',
        'menu_item_border_bottom' => '30px',
        'menu_item_border_color_hover' => 'rgb(208, 2, 27)',
        'menu_item_highlight_current' => 'off',
        'menu_item_divider_color' => 'rgb(222, 137, 137)',
        'panel_background_from' => 'rgb(235, 235, 235)',
        'panel_background_to' => 'rgb(235, 235, 235)',
        'panel_border_color' => 'rgb(225, 51, 73)',
        'panel_border_left' => '1px',
        'panel_border_right' => '1px',
        'panel_border_bottom' => '1px',
        'panel_header_color' => 'rgb(74, 74, 74)',
        'panel_header_font_size' => '13px',
        'panel_header_font_weight' => '300',
        'panel_header_padding_bottom' => '0px',
        'panel_header_border_color' => 'rgb(74, 74, 74)',
        'panel_padding_top' => '30px',
        'panel_padding_bottom' => '20px',
        'panel_widget_padding_top' => '5px',
        'panel_widget_padding_bottom' => '5px',
        'panel_font_size' => '14px',
        'panel_font_color' => '#666',
        'panel_font_family' => 'inherit',
        'panel_second_level_font_color' => 'rgb(74, 74, 74)',
        'panel_second_level_font_color_hover' => 'rgb(208, 2, 27)',
        'panel_second_level_text_transform' => 'uppercase',
        'panel_second_level_font' => 'inherit',
        'panel_second_level_font_size' => '14px',
        'panel_second_level_font_weight' => 'normal',
        'panel_second_level_font_weight_hover' => 'normal',
        'panel_second_level_text_decoration' => 'none',
        'panel_second_level_text_decoration_hover' => 'underline',
        'panel_second_level_background_hover_from' => 'rgba(241, 241, 241, 0)',
        'panel_second_level_background_hover_to' => 'rgba(241, 241, 241, 0)',
        'panel_second_level_border_color' => '#555',
        'panel_third_level_font_color' => 'rgb(208, 2, 27)',
        'panel_third_level_font_color_hover' => 'rgb(208, 2, 27)',
        'panel_third_level_font' => 'inherit',
        'panel_third_level_font_size' => '13px',
        'panel_third_level_font_weight_hover' => 'bold',
        'panel_third_level_text_decoration_hover' => 'underline',
        'panel_third_level_background_hover_from' => 'rgba(241, 241, 241, 0)',
        'panel_third_level_background_hover_to' => 'rgba(241, 241, 241, 0)',
        'flyout_width' => '230px',
        'flyout_menu_background_from' => 'rgba(241, 241, 241, 0)',
        'flyout_menu_background_to' => 'rgba(241, 241, 241, 0)',
        'flyout_border_right' => '10px',
        'flyout_menu_item_divider' => 'on',
        'flyout_menu_item_divider_color' => 'rgb(225, 51, 73)',
        'flyout_link_padding_bottom' => '10px',
        'flyout_background_from' => 'rgb(208, 2, 27)',
        'flyout_background_to' => 'rgb(208, 2, 27)',
        'flyout_background_hover_from' => 'rgb(190, 0, 23)',
        'flyout_background_hover_to' => 'rgb(190, 0, 23)',
        'flyout_link_size' => '14px',
        'flyout_link_color' => 'rgb(241, 241, 241)',
        'flyout_link_color_hover' => 'rgb(241, 241, 241)',
        'flyout_link_family' => 'inherit',
        'shadow_color' => 'rgb(221, 221, 221)',
        'toggle_background_from' => 'rgb(225, 51, 73)',
        'toggle_background_to' => 'rgb(225, 51, 73)',
        'mobile_background_from' => 'rgb(208, 2, 27)',
        'mobile_background_to' => 'rgb(208, 2, 27)',
        'mobile_menu_item_link_font_size' => '14px',
        'mobile_menu_item_link_color' => '#ffffff',
        'mobile_menu_item_link_text_align' => 'left',
        'mobile_menu_item_link_color_hover' => '#ffffff',
        'mobile_menu_item_background_hover_from' => 'rgb(208, 2, 27)',
        'mobile_menu_item_background_hover_to' => 'rgb(208, 2, 27)',
        'custom_css' => '/** Push menu onto new line **/ 
#{$wrap} { 
    clear: both; 
}',
        'sticky_menu_height' => 'on',
        'sticky_menu_item_link_height' => '40px',
        'tabbed_link_background_from' => 'rgb(190, 0, 23)',
        'tabbed_link_background_to' => 'rgb(190, 0, 23)',
        'tabbed_link_color' => 'rgb(221, 221, 221)',
        'tabbed_link_family' => 'inherit',
        'tabbed_link_size' => '14px',
        'tabbed_link_weight' => 'normal',
        'tabbed_link_padding_top' => '0px',
        'tabbed_link_padding_right' => '10px',
        'tabbed_link_padding_bottom' => '0px',
        'tabbed_link_padding_left' => '10px',
        'tabbed_link_height' => '35px',
        'tabbed_link_text_decoration' => 'none',
        'tabbed_link_text_transform' => 'none',
        'tabbed_link_background_hover_from' => 'rgb(208, 2, 27)',
        'tabbed_link_background_hover_to' => 'rgb(208, 2, 27)',
        'tabbed_link_weight_hover' => 'normal',
        'tabbed_link_text_decoration_hover' => 'none',
        'tabbed_link_color_hover' => 'rgb(255, 255, 255)',
        'tabbed_link_vertical_divider' => 'rgb(221, 221, 221)',
    );
    return $themes;
}
add_filter("megamenu_themes", "megamenu_add_theme_beadbuster_1547057362");

// Add this to your theme's functions.php
function codeslims_add_script_to_footer(){
    if( ! is_admin() ) { ?>
    <script>
   
jQuery(document).ready(function($){
$(document).on('click', '.plus', function(e) { // replace '.quantity' with document (without single quote)
    $input = $(this).prev('input.qty');
    var val = parseInt($input.val());
    var step = $input.attr('step');
    step = 'undefined' !== typeof(step) ? parseInt(step) : 1;
    $input.val( val + step ).change();
});
$(document).on('click', '.minus',  // replace '.quantity' with document (without single quote)
    function(e) {
    $input = $(this).next('input.qty');
    var val = parseInt($input.val());
    var step = $input.attr('step');
    step = 'undefined' !== typeof(step) ? parseInt(step) : 1;
    if (val > 0) {
        $input.val( val - step ).change();
    } 
});
});
</script>
<?php
    }
}
add_action( 'wp_footer', 'codeslims_add_script_to_footer' );

// Woocomerce Custom Field 
/*
 * You can use the following example to add virtually any custom fields 
 * to the checkout page. To more specifically control the location, use 
 * our hook guide: https://kb.checkoutwc.com/article/25-actions
 */
/**
 * Add the field to the checkout page
 */
add_action( 'woocommerce_after_order_notes', 'customise_checkout_field' );
function customise_checkout_field( $checkout ) {
	echo '<div id="greeting_card_field">';
	woocommerce_form_field(
		'greeting_card', array(
			'type'        => 'textarea',
			'class'       => array(
				'my-field-class form-row-wide',
			),
			'label'       => __( 'How did you hear about us?' ),
			'placeholder' => __( '' ), // If you want a placeholder, enter it here
			'required'    => true,
		), $checkout->get_value( 'customised_field_name' )
	);
	echo '</div>';
}
/*
 * Update value of field
 */
add_action( 'woocommerce_checkout_update_order_meta', 'customise_checkout_field_update_order_meta' );
function customise_checkout_field_update_order_meta( $order_id ) {
	if ( ! empty( $_POST['greeting_card'] ) ) {
		update_post_meta( $order_id, 'greeting_card', sanitize_text_field( $_POST['greeting_card'] ) );
	}
}