<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BeadBuster 2018
 */

?>
<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
	<meta charset="<?php bloginfo('charset');?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url');?>">

	<?php wp_head();?>

</head>

<body <?php body_class();?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e('Skip to content', 'beadbuster');?></a>

	<header class="site-header">
        <div class="wrap">
		<div class="site-branding">
			<?php the_custom_logo();?>
		</div><!-- .site-branding -->
		<div class="cart-icon off-canvas-open">
				<?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
		
		$count = WC()->cart->cart_contents_count;
		?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php 
		if ( $count > 0 ) {
			?>
			<span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
			<?php
		}
			?></a>

		<?php } ?>
		</div>
		<button type="button" class="off-canvas-open" aria-expanded="false" aria-label="<?php esc_html_e('Open Menu', 'beadbuster');?>">
			<span class="hamburger"></span>
		</button>

		<nav id="site-navigation" class="main-navigation">
			<?php
wp_nav_menu(array(
    'theme_location' => 'primary',
    'menu_id' => 'primary-menu',
    'menu_class' => 'menu dropdown',
));
?>
		</nav><!-- #site-navigation -->
		</div>
	</header><!-- .site-header-->

	<div id="content" class="site-content">

