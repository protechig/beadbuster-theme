<?php
/**
 * Template Name: Scaffolding
 *
 * Template Post Type: page, scaffolding, bbs__scaffolding
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BeadBuster 2018
 */

get_header(); ?>

	<div class="primary content-area">
		<main id="main" class="site-main">
			<?php do_action( 'bbs__scaffolding_content' ); ?>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
