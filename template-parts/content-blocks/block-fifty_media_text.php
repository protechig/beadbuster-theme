<?php
/**
 *  The template used for displaying fifty/fifty media/text.
 *
 * @package BeadBuster 2018
 */

// Set up fields.
$image_data = get_sub_field( 'media_left' );
$text = get_sub_field( 'text_primary' );
$animation_class = bbs__get_animation_class();
$header = get_sub_field( 'header' );

// Start a <container> with a possible media background.
bbs__display_block_options( array(
	'container' => 'section', // Any HTML5 container: section, div, etc...
	'class'     => 'content-block grid-container fifty-fifty fifty-media-text', // The class of the container.
) );
?>
	<?php if ( $header ) : ?>
	<div class="heading wrap">
		 <h2><?php echo esc_html( $header ); ?></h2>
		 <hr class="heading-underline" />
	</div><!-- .heading .wrap -->
	<?php endif; ?>

	<div class="grid-x <?php echo esc_attr( $animation_class ); ?>">
		<div class="cell image">
			<img class="fifty-image" src="<?php echo esc_url( $image_data['url'] ); ?>" alt="<?php echo esc_html( $image_data['alt'] ); ?>">
		</div>

		<div class="cell text">
			<?php
				echo force_balance_tags( $text ); // WPCS XSS OK.
			?>
		</div>
	</div><!-- .grid-x -->
</section><!-- .fifty-media-text -->
