<?php
/**
 *  The template used for displaying Envira Gallery.
 *
 * @package BeadBuster 2018
 */

 // Set up fields.

$header = get_sub_field( 'header_title' );

?>

<section class="envira-gallery grid-container content-block">
	<?php if ( $header ) : ?>
	<div class="heading wrap grid-x">
		<h2><?php the_sub_field( 'header_title' ); ?></h2>
		<hr class="heading-underline">
	</div>
	<?php endif; ?>

	<div class="gallery grid-x">    
		<?php
			$gallery_id = get_sub_field( 'the_gallery' );
			if ( $gallery_id != '' ) {
			if ( function_exists( 'envira_gallery' ) ) {
				envira_gallery( $gallery_id );
				}
			}
		?>
	</div>
</section>
