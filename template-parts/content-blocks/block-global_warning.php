<?php

bbs__display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'warning-massage', // Container class.
	)
);
?>
	<div class="grid-x">
		<div class="global-warning">
		<h3><i class="fas fa-exclamation-circle"></i></h3>
		<p><?php the_sub_field( 'warning_massage' ); ?></p>
		</div>
	</div>	
</section>
