<?php
/**
 *  The template used for displaying fifty/fifty media/text.
 *
 * @package BeadBuster 2018
 */

// Set up fields.

$animation_class = bbs__get_animation_class();

// Start a <container> with a possible media background.
bbs__display_block_options( array(
	'container' => 'section', // Any HTML5 container: section, div, etc...
	'class'     => 'content-block grid-container three-text', // The class of the container.
) );
?>
	<div class="grid-x <?php echo esc_attr( $animation_class ); ?>">

		<div class="cells">
			<?php the_sub_field( 'text_leftzz' ); ?>
			</div>
			<div class="cells">
			<?php the_sub_field( 'text_center' ); ?>
			</div>
			<div class="cells">
			<?php the_sub_field( 'text_right' ); ?>
		</div>

	</div><!-- .grid-x -->
</section><!-- .fifty-media-text -->
