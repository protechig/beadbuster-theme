<?php
/**
 *  The template used for displaying fifty/fifty text/text.
 *
 * @package BeadBuster 2018
 */

// Set up fields.
$text_primary = get_sub_field( 'text_primary' );
$text_secondary = get_sub_field( 'text_secondary' );
$animation_class = bbs__get_animation_class();
$header = get_sub_field( 'header' );

// Start a <container> with a possible media background.
bbs__display_block_options( array(
	'container' => 'section', // Any HTML5 container: section, div, etc...
	'class'     => 'content-block grid-container fifty-fifty fifty-text-only', // The container class.
) );
?>
	
	<?php if ( $header ) : ?>
	<div class="heading wrap">
		 <h2><?php echo esc_html( $header ); ?></h2>
		 <hr class="heading-underline" />
	</div><!-- .heading .wrap -->
	<?php endif; ?>

	<div class="grid-x <?php echo esc_attr( $animation_class ); ?>">

		<div class="cell">
			<?php
				echo force_balance_tags( $text_primary ); // WPCS: XSS OK.
			?>
		</div>

		<div class="cell">
			<?php
				echo force_balance_tags( $text_secondary ); // WPCS: XSS OK.
			?>
		</div>

	</div><!-- .grid-x -->
</section><!-- .fifty-text-only -->
