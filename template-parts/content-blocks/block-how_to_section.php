<?php

bbs__display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'how-to', // Container class.
	)
);
?>
<div class="wrap">
	<div class="demostration">
	<?php if (have_rows('bbs_services_section')):
    while (have_rows('bbs_services_section')): the_row();?>
	 <div class="options">
	      <img src="<?php the_sub_field('icon_image')['url'];?>" alt="<?php echo the_sub_field('icon_image')['alt']; ?>" />
		   <h3><?php the_sub_field('process');?></h3>
		   <div>
			<a class="button alternative" href="<?php the_sub_field( "button_link" ); ?>"><?php the_sub_field('button_text');?></a>
			</div>
	    </div>
			<?php endwhile;

 endif;
 ?>
	</div>
</div>

</section>
