<?php
/**
 *  The template used for displaying fifty/fifty media/text.
 *
 * @package BeadBuster 2018
 */

// Set up fields.

$animation_class = bbs__get_animation_class();

// Start a <container> with a possible media background.
bbs__display_block_options(array(
	'container' => 'section', // Any HTML5 container: section, div, etc...
	'class'     => 'content-block grid-container three-video', // The class of the container.
));
?>
	<div class=" grid-x video-header">
	<h3><?php the_sub_field( 'header' ); ?></h3>
	</div>
	<div class="grid-x <?php echo esc_attr( $animation_class ); ?>">
	
		<div class="cells">
		<div class="video">
			<?php the_sub_field( 'left_video' ); ?>
			</div>
			<p><?php the_sub_field( 'left_video_title' ); ?></p>
		</div>
		<div class="cells">
		<div class="video">
			<?php the_sub_field( 'center_video' ); ?>
			</div>
			<p><?php the_sub_field( 'center_video_title' ); ?></p>
		</div>
		<div class="cells">
		<div class="video">
			<?php the_sub_field( 'right_video' ); ?>
			</div>
			<p><?php the_sub_field( 'right_video_title' ); ?></p>
		</div>

	</div><!-- .grid-x -->
</section><!-- .fifty-media-text -->
