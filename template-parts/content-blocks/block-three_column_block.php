<?php
/**
 * The template used for displaying fifty/fifty blocks.
 *
 * @package BeadBuster 2018
 */

// Get the block layout field so block template is conditionally loaded.
$block_layout = get_sub_field( 'block_layout' );

switch ( $block_layout ) {

	case 'video_video_video':
		get_template_part( 'template-parts/content-blocks/block', 'video_video_video' );
		break;
	case 'text_text_text':
		get_template_part( 'template-parts/content-blocks/block', 'text_text_text' );
		break;

	default:
		get_template_part( 'template-parts/content-blocks/block', 'video_video_video' );
}


