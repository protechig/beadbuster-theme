<?php
/*
 * Carousel vidoe content block
 * 
 */

$title = get_sub_field( 'title' );
$logo = get_sub_field( 'logo' );
$slider = get_sub_field( 'slider' );
?>
    <!-- SLIDER BLOCK START -->
    <section class="content-block grid-container container slider-video-block">
        <!-- HEADING AREA START -->
        <div class="grid-x">
            <div class="cell">
                <h2><?php echo esc_html( $title ); ?></h2>
                
                <hr class="heading-underline">
            </div><!-- .cell -->

            <div class="cell">
                <?php if( !empty($logo) ): ?>
                <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" class="slider-logo">
                <?php endif; ?>
            </div><!-- .cell -->
        </div><!-- .grid-x -->
        <!-- HEADING AREA END -->
        <!-- SLIDER START -->
        <div class="grid-x">
            <div class="cell">
                <?php if (have_rows('slider')): ?>
                <div class="carousel">
                    <?php wp_enqueue_style('slick-carousel'); ?>

                    <?php wp_enqueue_script('slick-carousel'); ?>

                    <?php 
                        while( have_rows('slider') ) :
                            the_row();

                            $name = get_sub_field( 'name' );
                            $date = get_sub_field( 'date' );
                            $content = get_sub_field( 'content' );
                            $link = get_sub_field( 'link' );
                    ?>
                    
                    <div class="carousel-item">
                        <span class="content">
                           <?php the_sub_field( 'content' ); ?>
                        </span>

                        <?php
                        if( $link ): 
                            $link_url = $link['url'];
                            $link_title = $link['title'] ? $link['title'] : "Read More";
                            $link_target = $link['target'] ? $link['target'] : '_blank';	
                        ?>
                        <?php endif; ?>

                        <br>
                    </div><!-- .carousel-item -->
                    <?php endwhile; ?>
                </div><!-- .carousel -->
                <?php endif; ?>
            </div><!-- .cell -->
        </div><!-- .grid-x -->
        <!-- SLIDER END -->
    </section><!-- .content-block -->
    <!-- SLIDER BLOCK END -->

    <script>
        jQuery(document).ready(function() {	
            $('.carousel').slick({
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            adaptiveHeight: true
            });
        });
    </script>