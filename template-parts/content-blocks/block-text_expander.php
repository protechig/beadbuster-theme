<?php

bbs__display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'faqs', // Container class.
	)
);

$faqs_header = get_sub_field( 'faqs_header' );

?>
<div class=" faqs-content wrap">
 <div class="faqs-container">
	<?php if ( $faqs_header ) : ?>
		<h3 class="faqs-header"><?php the_sub_field( 'faqs_header' ); ?></h3>
	<?php endif; ?>
<?php
// Check if the repeater field has rows of data.
if ( have_rows( 'text_expander' ) ) :

// Loop through the rows of data.
	while ( have_rows( 'text_expander' ) ) :
		the_row();
?>
	<div class="faqs-container">
		     <div class="motor-icons">
			 <?php

				// check if the repeater field has rows of data
				if( have_rows('motor_type') ):

					// loop through the rows of data
					while ( have_rows('motor_type') ) : the_row();
					 ?>
					<img src="<?php the_sub_field('motor_icon_image'); ?>" alt="<?php the_sub_field('motor_icon_image'); ?>" />
					<?php
					endwhile;

				else :

					// no rows found

				endif;

				?>
			 </div>
			<div class="faqs-wraper">
			<div class="question">
			<i class="fas fa-caret-right" style="color: #d0021b;"></i>
			<h3 class="faqs-question">
			<?php
				// Display a sub field value.
				the_sub_field( 'questions' );
				?>
				</h3>
			</div>
			<div class="answer">
			<?php the_sub_field( 'answers' ); ?>
		</div>
		</div>
	</div>
	<?php
endwhile;

else :

// No rows found.
endif;

?>
</div>

</section>
