<section class="slider-section">
	<div class="wrap">
		<div class="slider">
<?php
$gallery_id = get_sub_field( 'the_slider' );
if ( $gallery_id != '' ) {
if ( function_exists( 'soliloquy' ) ) {
	soliloquy( $gallery_id );
}
}
?>

		</div>
		<div class="intro-text">
			<p><?php the_sub_field( 'bbs_parent_info' ); ?></p>
			<a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) );?>" class="button"><?php the_sub_field( 'bbs_shop_button' ); ?></a>
		</div> 
	</div><!-- .wrap -->
</section><!-- .slider-section -->
