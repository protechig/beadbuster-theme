<?php
/**
 * The template used for displaying page content in buddypress.php
 *
 * @package BeadBuster 2018
 */

?>
<div class="wrap">
<article <?php post_class(); ?>>

	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
