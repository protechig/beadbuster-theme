<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BeadBuster 2018
 */

?>
<header class="entry-header full-image">
<section class="hero" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>)">
	<div class="hero-content">
	<div class="entry-meta">
		<h3><?php echo get_the_category()[0]->name; ?></h3>
		</div>
		<?php the_title( '<h1 class="entry-title hero-title">', '</h1>' ); ?>
		<div class="entry-meta">
				<?php bbs__posted_on(); ?>
		</div><!-- .entry-meta -->
	</div>
</section>
</header>
<div class="wrap-single">
<article class="blog-post" <?php post_class(); ?>>

	<div class="entry-content">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'beadbuster' ), array(
					'span' => array(
						'class' => array(),
					),
				) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'beadbuster' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<?php

// Set up fields.
$title = get_field( 'title' );
$related_posts = get_field( 'related_posts' );
$animation_class = bbs__get_animation_class();

// Display section if we have any posts.
if ( $related_posts ) :

	// Start a <container> with possible block options.
	bbs__display_block_options(
		array(
			'container' => 'section', // Any HTML5 container: section, div, etc...
			'class'     => 'content-block grid-container related-posts', // Container class.
		)
	);

	?>

	<div class="grid-x">
	<?php if ( $title ) : ?>
		<h2 class="content-block-title"><?php echo esc_html( $title ); ?></h2>
	<?php endif; ?>
	</div>

	<div class="grid-x <?php echo esc_attr( $animation_class ); ?>">

		<?php
		// Loop through recent posts.
		foreach ( $related_posts as $key => $post ) :

			// Convert post object to post data.
			setup_postdata( $post );

			// Display a card.
			bbs__display_card( array(
				'id'       => get_the_ID(),
				'category' => true,
				'title'    => get_the_title(),
				'image'    => bbs__get_post_image_url( 'related-post-thumbnail' ),
				'text'     => bbs__get_the_excerpt( array(
					'length' => 5,
					'more'   => '...',
				) ),
				'url'      => get_the_permalink(),
				'class'    => 'cell',
			) );
		endforeach;
		wp_reset_postdata();
	?>
	</div><!-- .grid-x -->
</section><!-- .recent-posts -->
<?php endif; ?>
</article><!-- #post-## -->

