
<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Petey Greene
 */
if ( ! is_active_sidebar( 'footer-1' ) ) {
	return;
}
?>
<aside class="footer-widget-area widget-area" role="complementary">
	<div class="wrap">
		<div class="footer-widget footer-1">
		<?php dynamic_sidebar( 'footer-1' ); ?>
            <!-- this should be added in the widget footer1 -->
            <!-- <p><span class="info-icon"><i class="fas fa-envelope" style='color:#9b9b9b;'></i></span>example@email.com</p>
			<p><span class="info-icon"><i class="fas fa-phone" style='-webkit-transform: rotate(90deg);color:#9b9b9b;'></i></span>(123) 456 • 7890</p>
			<p><span class="info-icon"><i class="fas fa-map-marker-alt" style='color:#9b9b9b;'></i></span>1234 Example St.</p> -->

		</div>
		<div class="footer-widget footer-2">
			<?php dynamic_sidebar( 'footer-2' ); ?>	
		</div>
		<div class="footer-widget footer-3">
			<?php dynamic_sidebar( 'footer-3' ); ?>
		</div>
	</div>
</aside><!-- .secondary -->
