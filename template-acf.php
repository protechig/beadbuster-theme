<?php
/**
 * Template Name: Page with Content Blocks
 *
 * The template for displaying pages with ACF components.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BeadBuster 2018
 */

get_header(); ?>

	<div class="content-area">
		<main id="main" class="site-main">
			<?php 
			if ( is_front_page() ) {
				// This is the blog posts index
				echo '';
			} else {
				// This is not the blog posts index
				echo '<h1 class="entry-title">';
				echo esc_html( get_the_title() );
				echo '</h1>';
			}
			?>

			<?php bbs__display_content_blocks(); ?>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
