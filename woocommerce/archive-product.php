<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author         WooThemes
 * @package     WooCommerce/Templates
 * @version     3.3.0
 */
if (!defined('ABSPATH')) {
    exit;
}

get_header('shop');
?>
<div id="primary" class="content-area">

	<main id="main" class="site-main" role="main">
			<h1 class="entry-title"><?php echo esc_html( wp_title() ); ?></h1>
				<?php the_field('before_description', 'options'); ?>
			<div>
				<?php
				// WP_Term_Query arguments
				$args = array(
					'taxonomy' => array('product_cat'),
					'hierarchical' => false,
				);

				// The Term Query
				$term_query = new WP_Term_Query($args);

				// The Loop
				if (!empty($term_query) && !is_wp_error($term_query)) {
					$product_terms = $term_query->terms;
					foreach ($product_terms as $term) {
						?>
						<div class="category-product-wrapper row">
							<div class="product-image">
								<?php $thumbnail_id = get_woocommerce_term_meta($term->term_taxonomy_id, 'thumbnail_id', true);
								$image = wp_get_attachment_url($thumbnail_id);
								echo '<img src=' . $image . ' >';
								?>
							</div>
							<div class="product-content">
								<h2>
								<a href="<?php echo get_term_link($term->term_taxonomy_id); ?>" ><?php echo $term->name; ?></a>
								</h2>
								<p>
								<?php echo $term->description ?>
								</p>
								<a class="view " href="<?php echo get_term_link($term->term_taxonomy_id); ?>">Learn More >></a>
							</div>
						</div>
						<?php //var_dump($term) ?>
						<?php
				}
					// do something
				} else {
					// no terms found
				}
				?>
			</div>
	</main>
</div>
<?php
get_footer('shop');
